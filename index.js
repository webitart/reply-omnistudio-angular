const express = require('express')
var path = require('path');
const app = express();

const PORT = 8080
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
// setup public folder
app.use(express.static('public/dist'));

// Manage routes
app.get('/',function (req, res) {
    res.sendFile(path.join(__dirname+'/public/dist/index.html'));
});

app.listen(PORT, () => console.log(`Omniout app Started on port ${PORT}!`));