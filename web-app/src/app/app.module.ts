import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LwcComponent } from './lwc/lwc.component';

import {WindowRef} from './WindowRef';

@NgModule({
  declarations: [
    AppComponent,
    LwcComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  providers: [WindowRef],
  bootstrap: [AppComponent]
})
export class AppModule { }
