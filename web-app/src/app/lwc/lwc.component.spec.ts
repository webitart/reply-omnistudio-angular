import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LwcComponent } from './lwc.component';

describe('LwcComponent', () => {
  let component: LwcComponent;
  let fixture: ComponentFixture<LwcComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LwcComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LwcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
