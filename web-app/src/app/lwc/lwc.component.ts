import { Component, OnInit } from '@angular/core';

import {WindowRef} from './../WindowRef';

@Component({
  selector: 'app-lwc',
  templateUrl: './lwc.component.html',
  styleUrls: ['./lwc.component.css']
})
export class LwcComponent implements OnInit {

  constructor(private winRef: WindowRef) {
    const {ReplyHelloWorldEnglish} = winRef.nativeWindow.lwcComponents

    if (!customElements.get('vlocityomniscript-app-component')) {
      customElements.define('vlocityomniscript-app-component', ReplyHelloWorldEnglish.CustomElementConstructor);
    }

  }

  ngOnInit(): void {

  }

}
